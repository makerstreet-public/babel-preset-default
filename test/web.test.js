'use strict'
const { transform } = require('./test-utils')

let webPreset = require('../web')
let nodePreset = require('../node')

const testFiles = [
  'async-await',
  'class-properties',
  'export-default-from',
  'export-namespace-from',
  'general-syntax',
  'generators',
  'nullish-coalescing-operator',
  'optional-chaining',
]
const testPresets = [
  ['@touchtribe/babel-presets/web', webPreset, {}],
  ['@touchtribe/babel-presets/web:legacy', webPreset, { isLegacy: true }],
  ['@touchtribe/babel-presets/web:alias', webPreset, { alias: { app: '../' } }],
  ['@touchtribe/babel-presets/node', nodePreset, {}],
]
testPresets.forEach((entry) => {
  const [
    testTitle,
    preset,
    options,
  ] = entry
  describe(testTitle, () => {
    const presets = [[preset, options]]

    for (let fileName of testFiles) {
      test(fileName.replace(/-/g, ' '), (done) => {
        transform(fileName, presets)
          .then(output => {
            expect(output).toMatchSnapshot()
            done()
          })
      })
    }
  })
})

