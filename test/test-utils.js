let babel = require('@babel/core')
let fs = require('mz/fs')

process.chdir(__dirname)
process.env.BABEL_ENV = process.env.NODE_ENV = 'production'

async function transform (fileName, presets) {
  const input = await fs.readFile(`./fixture/${fileName}.js`)
  return babel.transform(input, { presets }).code
}

module.exports = {
  transform
}
