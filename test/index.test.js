'use strict'
const { transform } = require('./test-utils')

const testFiles = [
  'general-syntax'
]
const testPresets = [
  ['@touchtribe/babel-presets/', require('../index')],
  ['@touchtribe/babel-presets/web', require('../web')],
  ['@touchtribe/babel-presets/node', require('../node')],
  ['@touchtribe/babel-presets/react', require('../react')],
]

testPresets.forEach((entry) => {
  const [
    testTitle,
    preset,
    options,
  ] = entry
  describe(testTitle, () => {
    const presets = [[preset, options]]

    for (let fileName of testFiles) {
      test(fileName.replace(/-/g, ' '), (done) => {
        transform(fileName, presets)
          .then(output => {
            expect(output).toMatchSnapshot()
            done()
          })
      })
    }
  })
})

