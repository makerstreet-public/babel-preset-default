const { declare } = require('@babel/helper-plugin-utils')
const { ifUndefined } = require('./utils')
const { nodeTargets } = require('./config')

const createDefaultPreset = require('./index.js')

function createPreset (api, options) {
  options = {
    ...options,
    targets: ifUndefined(options.targets, nodeTargets),
    modules: ifUndefined(options.modules, false)
  }

  return createDefaultPreset(api, options)
}

module.exports = declare((api, options) => {
  api.assertVersion(7)

  return createPreset(api, options)
})
