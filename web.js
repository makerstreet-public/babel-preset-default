const { declare } = require('@babel/helper-plugin-utils')
const { validateBoolOption, ifUndefined } = require('./utils')
const { targets, legacyTargets } = require('./config')

const createDefaultPreset = require('./index.js')

function createPreset (api, options) {
  const isLegacy = validateBoolOption('isLegacy', options.isLegacy, false)

  options = {
    ...options,
    targets: ifUndefined(options.targets, isLegacy ? legacyTargets : targets)
  }

  return createDefaultPreset(api, options)
}

module.exports = declare((api, options) => {
  api.assertVersion(7)

  return createPreset(api, options)
})
