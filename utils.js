function validateBoolOption (name, value, defaultValue) {
  if (typeof value === 'undefined') {
    value = defaultValue
  }

  if (typeof value !== 'boolean') {
    throw new Error(`@touchtribe/babel-presets: '${name}' option must be a boolean.`)
  }

  return value
}

function getEnv () {
  const env = process.env.NODE_ENV || process.env.BABEL_ENV || 'development'

  return {
    isDevelopment: env === 'development',
    isTest: env === 'test',
    isProduction: env === 'production'
  }
}

function ifUndefined (val, defaultValue) {
  return typeof val === 'undefined'
    ? defaultValue
    : val
}

module.exports = {
  validateBoolOption,
  getEnv,
  ifUndefined
}
