const targets = {
  esmodules: false,
  browsers: [
    'last 2 Chrome versions',
    'not Chrome < 60',
    'last 2 Safari versions',
    'not Safari < 10.1',
    'last 2 iOS versions',
    'not iOS < 10.3',
    'last 2 Firefox versions',
    'not Firefox < 54',
    'last 2 Edge versions',
    'not Edge < 15'
  ]
}
const legacyTargets = {
  esmodules: false,
  browsers: [
    '> 1%',
    'last 2 versions',
    'Firefox ESR'
  ]
}
const nodeTargets = {
  node: 'current'
}
const testTargets = {
  node: 'current'
}

const presetEnvConfig = {
  useBuiltIns: 'usage'
}

const defaultPlugins = [
  // https://babeljs.io/docs/en/next/babel-plugin-proposal-class-properties.html
  // https://github.com/tc39/proposal-class-fields
  '@babel/plugin-proposal-class-properties',

  // https://babeljs.io/docs/en/babel-plugin-proposal-export-default-from
  // https://github.com/tc39/proposal-export-default-from
  '@babel/plugin-proposal-export-default-from',

  // https://babeljs.io/docs/en/babel-plugin-proposal-export-namespace-from
  // https://github.com/tc39/proposal-export-ns-from
  '@babel/plugin-proposal-export-namespace-from',

  // https://babeljs.io/docs/en/next/babel-plugin-proposal-nullish-coalescing-operator.html
  // https://github.com/tc39/proposal-nullish-coalescing
  '@babel/plugin-proposal-nullish-coalescing-operator',

  // https://babeljs.io/docs/en/babel-plugin-proposal-optional-chaining
  // https://github.com/tc39/proposal-optional-chaining
  '@babel/plugin-proposal-optional-chaining',

  // https://babeljs.io/docs/en/babel-plugin-syntax-dynamic-import
  //
  '@babel/plugin-syntax-dynamic-import',

  '@babel/plugin-transform-runtime'
]

module.exports = {
  defaultPlugins,
  legacyTargets,
  nodeTargets,
  presetEnvConfig,
  targets,
  testTargets
}
